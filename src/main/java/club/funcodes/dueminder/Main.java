// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is written and provided by Siegfried Steiner, Munich, Germany.
// Feel free to use it as skeleton for your own applications. Make sure you have
// considered the license conditions of the included artifacts (pom.xml).
// -----------------------------------------------------------------------------
// The REFCODES.ORG artifacts used by this template are copyright (c) by
// Siegfried Steiner, Munich, Germany and licensed under the following
// (see "http://en.wikipedia.org/wiki/Multi-licensing") licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.dueminder;

import static org.refcodes.cli.CliSugar.*;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.ConfigOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.StringOption;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;

/**
 * A minimum REFCODES.ORG enabled command application processing CSV files. Get
 * inspired by "https://bitbucket.org/funcodez".
 */
public class Main {

	// See "http://www.refcodes.org/blog/logging_like_the_nerds_log" |-->
	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();
	// <--| See "http://www.refcodes.org/blog/logging_like_the_nerds_log"

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "dueminder";
	private static final String TITLE = ( NAME.lastIndexOf( '-' ) != -1 ? "<" + NAME.substring( NAME.lastIndexOf( '-' ) + 1 ) + ">" : NAME ).toUpperCase();
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String DESCRIPTION = "Sends annual dates reminder E-Mails configured in a CSSV file. Get inspired by [https://bitbucket.org/funcodez].";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES (see [https://www.funcodes.club])";
	private static final String DUE_DATES_FILE = "dueDatesFile";
	// private static final char DEFAULT_DELIMITER = Delimiter.CSV.getChar();

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		final StringOption theDueDatesFileArg = stringOption( 'd', "due-dates-file", DUE_DATES_FILE, "The source CSV file from which to read the annual dates." );
		final ConfigOption theConfigPathArg = configOption( "The configuration file (absolute path or relative) to use." );
		final Flag theDaemonFlag = daemonFlag();
		final Flag theInitFlag = initFlag( false );
		final Flag theQuietFlag = quietFlag();
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();
		final Flag theDebugFlag = debugFlag();

		// @formatter:off
		final Term theArgsSyntax = cases (
			optional( theDueDatesFileArg, theDaemonFlag, theQuietFlag, theConfigPathArg, theDebugFlag),
			and( theInitFlag, optional( theConfigPathArg, theQuietFlag ) ),
			xor( theHelpFlag, and( theSysInfoFlag, any( theQuietFlag) ) ) 
		);
		final Example[] theExamples = examples(
			example( "Process default annual dates file" ), 
			example( "Process source annual dates file", theDueDatesFileArg ), 
			example( "To show the help text", theHelpFlag ), 
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		// final boolean isVerbose = theCliHelper.isVerbose();

		try {

		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}
	}
}
